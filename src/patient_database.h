#ifndef PATIENTDATABASEFASADE_H
#define PATIENTDATABASEFASADE_H

#include <QString>
#include <QObject>


class QSqlDatabase;
class QAbstractItemModel;
class QByteArray;
class QSqlQuery;

namespace DBPW
{
class PatientDatabase : public QObject
{
	Q_OBJECT
public:
	static PatientDatabase& instance();

	bool addPatient( const QString& strSurname,
	                 const QString& strName,
	                 const QString& strPatronymic,
	                 const QString& strBirthday,
	                 const QString& strAddress );
	bool removePatient(const uint32_t idPatient);

	bool dischargePatient(const uint32_t idPatient);
	bool adoptPatient(const uint32_t idPatient);

	bool addSnapshot(const uint32_t idPatient, const QByteArray& buffer );
	bool removeSnapshot(const uint32_t idSnapshot );

	QAbstractItemModel* getPatientListAll();
	QAbstractItemModel* getPatientListNotDischarged();
	QAbstractItemModel* getExaminationHistoryByPatient( uint32_t idPatient );

	QAbstractItemModel* getSnapshotListByPatientId(const uint32_t idSnapshot);
	QByteArray getSnapshotById( const uint32_t idSnapshot );

	bool isConnectionOpen();
	bool openConnection( const QString& strDatabaseName );
	void closeConnection();

private:
	QSqlDatabase* m_database;

	PatientDatabase();
	PatientDatabase( const PatientDatabase& ) = delete;
	PatientDatabase& operator=( const PatientDatabase& ) = delete;

	QAbstractItemModel* executeSelectQuery( QSqlQuery& query );
};
}

#endif // PATIENTDATABASEFASADE_H
