#include "add_patient_form.h"
#include "ui_add_patient_form.h"
#include "patient_database.h"
#include <QMessageBox>

namespace DBPW
{
AddPatientForm::AddPatientForm( QWidget *parent ) :
    QDialog( parent ),
    ui( new Ui::AddPatientForm ),
    m_codeNotAdded( -1998 ),
    m_isNewPatientAdded( false )
{
	ui->setupUi( this );
	setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint);

	connect( ui->pbAdd, &QPushButton::clicked, this, &AddPatientForm::addButtonClicked );
	connect( ui->pbAddNext, &QPushButton::clicked, this, &AddPatientForm::addNextButtonClicked );
	connect( ui->pbBack, &QPushButton::clicked, this, &AddPatientForm::backButtonClicked );
}

AddPatientForm::~AddPatientForm()
{
	delete ui;
}

void AddPatientForm::addButtonClicked()
{
	if( addPatient() )
		done( 0 );
}

void AddPatientForm::addNextButtonClicked()
{
	if( addPatient() )
	{
		clearFields();
		m_isNewPatientAdded = true;
	}
}

void AddPatientForm::backButtonClicked()
{
	if( m_isNewPatientAdded )
		done( 0 );
	else
		done( m_codeNotAdded );
}

bool AddPatientForm::hasEmptyFields()
{
	if( ui->leSurname->text().isEmpty() )
		return true;
	if( ui->leName->text().isEmpty() )
		return true;
	if( ui->lePatronymic->text().isEmpty() )
		return true;
	if( ui->leAddress->text().isEmpty() )
		return true;
	return false;
}

void AddPatientForm::clearFields()
{
	ui->leSurname->clear();
	ui->leName->clear();
	ui->lePatronymic->clear();
	ui->leAddress->clear();
}

bool AddPatientForm::addPatient()
{
	if( hasEmptyFields() )
	{
		QMessageBox::critical( this, tr("Error"), tr("All fields must be filled!") );
		return false;
	}

	QDate dateBirthday = ui->cwBirthday->selectedDate();
	QString strMonth, strDay;
	int monthNumber = dateBirthday.month();
	int dayNumber = dateBirthday.day();
	if( monthNumber / 10 == 0)
		strMonth = "0" + QString::number(monthNumber);
	else
		strMonth = QString::number( monthNumber );
	if( dayNumber / 10 == 0)
		strDay = "0" + QString::number( dayNumber );
	else
		strDay = QString::number( dayNumber );
	QString date = QString( "%1-%2-%3" ).arg( QString::number( dateBirthday.year() ), strMonth, strDay );

	bool result = DBPW::PatientDatabase::instance().addPatient( ui->leSurname->text(),
	                                                            ui->leName->text(),
	                                                            ui->lePatronymic->text(),
	                                                            date,
	                                                            ui->leAddress->text() );
	if( !result )
	{
		QMessageBox::critical( this, tr("Error"), tr("Patient not added!") );
		return false;
	}
	return true;

}
}
