#ifndef IMAGE_VIEWER_H
#define IMAGE_VIEWER_H

#include <QDialog>

namespace Ui
{
class SnapshotViewer;
}

namespace DBPW
{
class SnapshotViewer :
        public QDialog
{
	Q_OBJECT
public:
	explicit SnapshotViewer(const QPixmap& pixmap, QWidget *parent = nullptr );
	~SnapshotViewer();

protected:
	void wheelEvent( QWheelEvent* event ) override;
	void resizeEvent( QResizeEvent* event ) override;

private:
	Ui::SnapshotViewer *ui;
	QPixmap m_pixmapSnapshot;
	double m_factorScaleInStep;
	double m_factorScaleOutStep;
	double m_factorScaleMax;
	double m_factorScaleMin;
	double m_factorScaleCurrent;


	void scaleImage( double factor );
};
}
#endif // IMAGE_VIEWER_H
