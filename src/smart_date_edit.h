#ifndef SMARTDATEEDIT_H
#define SMARTDATEEDIT_H

#include <QDateEdit>

class QToolButton;
class QPushButton;

namespace DBPW
{
class SmartDateEdit :
        public QDateEdit
{
	Q_OBJECT
public:
	explicit SmartDateEdit(QWidget *parent = 0);

	QDateTime dateTime() const;
	QDate date() const;
	QTime time() const;

	QSize sizeHint() const;
	QSize minimumSizeHint() const;

signals:
	void textDateChanged( const QString& text);

public Q_SLOTS:
	void setDateTime( const QDateTime &dateTime );
	void setDate( const QDate &date );
	void setTime( const QTime &time );

protected:
	void showEvent( QShowEvent *event );
	void resizeEvent( QResizeEvent *event );
	void keyPressEvent( QKeyEvent *event );
	void mousePressEvent( QMouseEvent *event);
	bool focusNextPrevChild( bool next);
	void focusInEvent( QFocusEvent* event);
	void focusOutEvent( QFocusEvent* event );
	QValidator::State validate( QString &input, int &pos ) const;

private slots:
	void clearButtonClicked();

private:
	QPushButton* m_pbClearText;
	bool m_isNull;

	void setNull( bool status );
};
}
#endif // SMARTDATEEDIT_H
