#include "main_window.h"
#include "ui_main_window.h"

#include <QMessageBox>
#include <QSortFilterProxyModel>
#include <QFileDialog>
#include <QStandardItemModel>
#include <QBuffer>
#include <QDebug>
#include <QSqlQueryModel>
#include <QTranslator>

#include "patient_database.h"
#include "add_patient_form.h"
#include "snapshot_viewer.h"

namespace DBPW
{
MainWindow::MainWindow( QWidget *parent ) :
    QMainWindow( parent ),
    ui( new Ui::MainWindow ),
    m_translator( new QTranslator ),
    m_sfpModelId( new QSortFilterProxyModel ),
    m_sfpModelSurname( new QSortFilterProxyModel ),
    m_sfpModelName( new QSortFilterProxyModel ),
    m_sfpModelPatronymic( new QSortFilterProxyModel ),
    m_sfpModelBirthday( new QSortFilterProxyModel ),
    m_sfpModelAddress( new QSortFilterProxyModel ),
    m_siModelSnapshot( new QStandardItemModel ),
    m_idPatientSelected( 0 ),
    m_isFullPatientList( true ),
    m_isLightTheme( true )
{
	ui->setupUi( this );
	changeThemeButtonClicked();
	setRussianLanguageAction();

	ui->deBirthday->setDate( {} );
	ui->lvSnapshots->setModel( m_siModelSnapshot );

	initSortFilterProxyModels();
	ui->tvPatients->setModel( m_sfpModelId );
	showAllPatientsAction();
	ui->actionAddSnapshot->setVisible( false );

	connect( ui->actionAddPatient, &QAction::triggered, this, &MainWindow::addPatientAction );
	connect( ui->actionAddSnapshot, &QAction::triggered, this, &MainWindow::addSnapshotAction );
	connect( ui->actionShowAll, &QAction::triggered, this, &MainWindow::showAllPatientsAction );
	connect( ui->actionShowNotDischarged, &QAction::triggered, this, &MainWindow::showNotDischargedPatientsAction );
	connect( ui->leSearchBySurname, &QLineEdit::textChanged, this, &MainWindow::searchSurnameTextChanged );
	connect( ui->leSearchByName, &QLineEdit::textChanged, this, &MainWindow::searchNameTextChanged );
	connect( ui->leSearchByPatronymic, &QLineEdit::textChanged, this, &MainWindow::searchPatronymicTextChanged );
	connect( ui->deBirthday, &SmartDateEdit::dateChanged, this, &MainWindow::searchBirthdayTextChanged );
	connect( ui->leSearchByAddress, &QLineEdit::textChanged, this, &MainWindow::searchAddressTextChanged );
	connect( ui->lvSnapshots, &QListView::doubleClicked, this, &MainWindow::snapshotDoubleClicked );
	connect( ui->tvPatients->selectionModel(), &QItemSelectionModel::selectionChanged, this, &MainWindow::patientSelected );
	connect( ui->tvPatients, &QTableView::customContextMenuRequested, this, &MainWindow::showPatientContextMenu );
	connect( ui->lvSnapshots, &QTableView::customContextMenuRequested, this, &MainWindow::showSnapshotContextMenu );
	connect( ui->actionSetEnglish, &QAction::triggered, this, &MainWindow::setEnglishLanguageAction );
	connect( ui->actionSetRussian, &QAction::triggered, this, &MainWindow::setRussianLanguageAction );
	connect( ui->tbChangeTheme, &QToolButton::clicked, this, &MainWindow::changeThemeButtonClicked );
}

MainWindow::~MainWindow()
{
	delete m_translator;
	delete ui;
}

void MainWindow::changeEvent( QEvent* event )
{
	if (event->type() == QEvent::LanguageChange)
	{
		auto model = ui->tvPatients->model();
		if( model )
		{
			model->setHeaderData( 0, Qt::Horizontal, tr("ID") );
			model->setHeaderData( 1, Qt::Horizontal, tr("Surname") );
			model->setHeaderData( 2, Qt::Horizontal, tr("Name") );
			model->setHeaderData( 3, Qt::Horizontal, tr("Patronymic") );
			model->setHeaderData( 4, Qt::Horizontal, tr("Birthday") );
			model->setHeaderData( 5, Qt::Horizontal, tr("Address") );
		}
		model = ui->tvExaminations->model();
		if( model )
		{
			model->setHeaderData( 0, Qt::Horizontal, tr("ID") );
			model->setHeaderData( 1, Qt::Horizontal, tr("Addmission date") );
			model->setHeaderData( 2, Qt::Horizontal, tr("Discharge date") );
		}
		ui->retranslateUi( this );
	}
}

void MainWindow::patientSelected( const QItemSelection& selected, const QItemSelection& deselected )
{
	if( selected.size() != 0)
	{
		m_idPatientSelected = selected.begin()->topLeft().data().toUInt();
		loadExamitationData();
		loadSnapshotData();
		ui->actionAddSnapshot->setVisible( true );
	}
	else if( deselected.size() != 0 )
	{
		ui->tvPatients->clearSelection();
		dynamic_cast<QSqlQueryModel*>( ui->tvExaminations->model() )->clear();
		m_siModelSnapshot->clear();
		ui->actionAddSnapshot->setVisible( false );
	}
}

void MainWindow::addPatientAction()
{
	AddPatientForm dialog( this );
	int result = dialog.exec();
	if( result == 0 )
	{
		if( m_isFullPatientList )
			showAllPatientsAction();
		else
			showNotDischargedPatientsAction();
	}
}

void MainWindow::removePatientAction()
{
	bool result = PatientDatabase::instance().removePatient( m_idPatientSelected );
	if( !result )
		QMessageBox::critical( this, tr("Error"), tr("Patient didn't remove!") );
	else
	{
		if( m_isFullPatientList )
			showAllPatientsAction();
		else
			showNotDischargedPatientsAction();
	}
}

void MainWindow::dischargePatientAction()
{
	bool result = PatientDatabase::instance().dischargePatient( m_idPatientSelected );
	if( !result )
		QMessageBox::critical( this, tr("Error"), tr("Patient didn't discharge!") );
	else
	{
		ui->actionAdopt->setEnabled( true );
		ui->actionDischarge->setEnabled( false );

		if( !m_isFullPatientList )
			showNotDischargedPatientsAction();
		else
			loadExamitationData();
	}
}
void MainWindow::adoptPatientAction()
{
	bool result = PatientDatabase::instance().adoptPatient( m_idPatientSelected );
	if( !result )
		QMessageBox::critical( this, tr("Error"), tr("Patient didn't adopt!") );
	else
	{
		ui->actionAdopt->setEnabled( false );
		ui->actionDischarge->setEnabled( true );

		loadExamitationData();
	}
}

void MainWindow::showAllPatientsAction()
{
	auto model = DBPW::PatientDatabase::instance().getPatientListAll();
	if( !model )
		return;

	loadPatientData( model );

	ui->lPatientView->setText( tr("All patients") );
	m_isFullPatientList = true;

	m_siModelSnapshot->clear();
	if( ui->tvExaminations->model() )
		dynamic_cast<QSqlQueryModel*>(ui->tvExaminations->model())->clear();
}

void MainWindow::showNotDischargedPatientsAction()
{
	QAbstractItemModel* model = PatientDatabase::instance().getPatientListNotDischarged();
	if( !model )
		return;

	loadPatientData(model);
	ui->lPatientView->setText( tr("Not discharged patients") );
	m_isFullPatientList = false;

	m_siModelSnapshot->clear();
	if( ui->tvExaminations->model() )
		dynamic_cast<QSqlQueryModel*>(ui->tvExaminations->model())->clear();
}

void MainWindow::addSnapshotAction()
{
	if ( m_idPatientSelected == 0 )
		return;
	QString strFileName = QFileDialog::getOpenFileName( this, tr("Open file"), tr(""), "Image Files (*.jpg)" );
	if( strFileName.isEmpty() )
		return;
	QPixmap pmSnapshot;
	if ( !pmSnapshot.load(strFileName) )
	{
		QMessageBox::critical( this, tr("Error"), tr("Failed to upload snapshot!") );
		return;
	}
	QByteArray inByteArray;
	QBuffer inBuffer( &inByteArray );
	inBuffer.open( QIODevice::WriteOnly );
	pmSnapshot.save( &inBuffer, "JPG" );

	if ( !PatientDatabase::instance().addSnapshot( m_idPatientSelected, inByteArray ) )
		QMessageBox::critical( this, tr("Error"), tr("Failed to add snapshot!") );
	else
		loadSnapshotData();
}

void MainWindow::removeSnapshotAction()
{
	auto listItemSelected = ui->lvSnapshots->selectionModel()->selectedIndexes();

	uint32_t idSnapshot = listItemSelected[0].data( Qt::ToolTipRole ).toUInt() ;
	if ( !PatientDatabase::instance().removeSnapshot( idSnapshot ) )
		QMessageBox::critical( this, tr("Error"), tr("Failed to delete snapshot!") );
	else
		m_siModelSnapshot->removeRow( listItemSelected[0].row() );
}

void MainWindow::setRussianLanguageAction()
{
	m_translator->load( ":/languages/ru" );
	qApp->installTranslator( m_translator );
	ui->actionSetRussian->setChecked( true );
	ui->actionSetEnglish->setChecked( false );
}

void MainWindow::setEnglishLanguageAction()
{
	m_translator->load( ":/languages/en" );
	qApp->installTranslator( m_translator );
	ui->actionSetRussian->setChecked( false );
	ui->actionSetEnglish->setChecked( true );
}

void MainWindow::changeThemeButtonClicked()
{
	if( m_isLightTheme )
	{
		updateTheme( ":/styles/dark" );
		ui->tbChangeTheme->setIcon( QIcon( QPixmap(":/icons/light" ) ) );
	}
	else
	{
		updateTheme( "" );
		ui->tbChangeTheme->setIcon( QIcon( QPixmap( ":/icons/dark" ) ) );
	}
	m_isLightTheme = !m_isLightTheme;
}

void MainWindow::loadPatientData( QAbstractItemModel* model )
{
	auto modelPatientOld = m_sfpModelAddress->sourceModel();
	m_sfpModelAddress->setSourceModel( model );
	delete modelPatientOld;

	ui->tvPatients->setColumnWidth(0, 40);
	m_idPatientSelected = 0;
}

void MainWindow::loadExamitationData()
{
	auto model = DBPW::PatientDatabase::instance().getExaminationHistoryByPatient( m_idPatientSelected );
	if( !model )
		return;
	auto modelExaminationOld = ui->tvExaminations->model();
	ui->tvExaminations->setModel( model );
	delete modelExaminationOld;
	ui->tvExaminations->setColumnWidth(0, 40);
	ui->tvExaminations->setColumnWidth(1, (ui->tvExaminations->width() - ui->tvExaminations->columnWidth(0)) / 2);
}

void MainWindow::loadSnapshotData()
{
	m_siModelSnapshot->clear();
	QAbstractItemModel* model = DBPW::PatientDatabase::instance().getSnapshotListByPatientId( m_idPatientSelected );
	if( model != nullptr )
	{
		int rowCount = model->rowCount();
		if( rowCount != 0 )
		{
			QStandardItem* lwiSnapshot = nullptr;
			QByteArray bufferImage;
			QPixmap pixmapTemp;
			QSize sizeIcon = ui->lvSnapshots->iconSize();
			for( int i = 0; i < rowCount; ++i )
			{
				bufferImage = model->data( model->index(i, 1) ).toByteArray();
				if( !pixmapTemp.loadFromData( bufferImage ) )
					continue;

				lwiSnapshot = new QStandardItem();
				lwiSnapshot->setSizeHint( {sizeIcon.width() + 10, sizeIcon.height() + 20} );
				lwiSnapshot->setIcon( QIcon( pixmapTemp ) );
				lwiSnapshot->setTextAlignment( Qt::AlignHCenter | Qt::AlignBottom );
				lwiSnapshot->setText( model->data( model->index( i, 2 ) ).toString() );
				lwiSnapshot->setToolTip( QString::number( model->data( model->index( i, 0 ) ).toUInt() ) );
				m_siModelSnapshot->appendRow( lwiSnapshot );
			}
		}
		delete model;
	}
}

void MainWindow::searchNameTextChanged( const QString& text )
{
	m_sfpModelName->setFilterRegExp( text );
}

void MainWindow::searchSurnameTextChanged( const QString& text )
{
	m_sfpModelSurname->setFilterRegExp( text );
}

void MainWindow::searchPatronymicTextChanged( const QString& text )
{
	m_sfpModelPatronymic->setFilterRegExp( text );
}

void MainWindow::searchBirthdayTextChanged( const QDate& date )
{
	QString strDate = ui->deBirthday->findChild<QLineEdit*>()->text();
	m_sfpModelBirthday->setFilterRegExp( strDate );
}

void MainWindow::searchAddressTextChanged( const QString& text )
{
	m_sfpModelAddress->setFilterRegExp( text );
}

void MainWindow::snapshotDoubleClicked( const QModelIndex& item )
{
	uint32_t idSnapshot = item.data( Qt::ToolTipRole ).toUInt() ;
	QByteArray bufferImage = PatientDatabase::instance().getSnapshotById( idSnapshot );
	if ( !bufferImage.isEmpty() )
	{
		QPixmap pmSnapshot = QPixmap();
		pmSnapshot.loadFromData( bufferImage );
		SnapshotViewer viewSnapshot( pmSnapshot, this );
		viewSnapshot.exec();
	}
}

void MainWindow::showPatientContextMenu( const QPoint& pos )
{
	QMenu menuContext;
	QPoint pointGlobal = ui->tvPatients->mapToGlobal( pos );

	auto listItemSelected = ui->tvPatients->selectionModel()->selectedRows();
	if( ui->tvPatients->indexAt( pos ).isValid() && listItemSelected.size() != 0)
	{
		menuContext.addAction( tr("Remove"), this, &MainWindow::removePatientAction );

		QString strDateDischarge = ui->tvExaminations->model()->index(0, 2).data().toString();
		if( strDateDischarge.isEmpty() )
			menuContext.addAction( tr("Discharge"), this, &MainWindow::dischargePatientAction );
		else
			menuContext.addAction( tr("Adopt"), this, &MainWindow::adoptPatientAction );
	}
	else
	{
		ui->tvPatients->clearSelection();
		menuContext.addAction( tr("Add"), this, &MainWindow::addPatientAction );
	}
	menuContext.exec( pointGlobal );
}

void MainWindow::showSnapshotContextMenu( const QPoint& pos )
{
	auto listItemSelected = ui->tvPatients->selectionModel()->selectedIndexes();
	if( listItemSelected.size() == 0 )
		return;

	QMenu menuContext;
	QPoint pointGlobal = ui->lvSnapshots->mapToGlobal( pos );

	if( ui->lvSnapshots->indexAt( pos ).isValid() && ui->lvSnapshots->selectionModel()->selectedRows().size() != 0)
	{
		menuContext.addAction( tr("Remove"), this, &MainWindow::removeSnapshotAction );
	}
	else
	{
		ui->lvSnapshots->clearSelection();
		menuContext.addAction( tr("Add"), this, &MainWindow::addSnapshotAction );
	}
	menuContext.exec( pointGlobal );
}

void MainWindow::initSortFilterProxyModels()
{
	m_sfpModelBirthday->setSourceModel( m_sfpModelAddress );
	m_sfpModelPatronymic->setSourceModel( m_sfpModelBirthday );
	m_sfpModelName->setSourceModel( m_sfpModelPatronymic );
	m_sfpModelSurname->setSourceModel( m_sfpModelName );
	m_sfpModelId->setSourceModel( m_sfpModelSurname );

	m_sfpModelId->setFilterKeyColumn( 0 );
	m_sfpModelSurname->setFilterKeyColumn( 1 );
	m_sfpModelName->setFilterKeyColumn( 2 );
	m_sfpModelPatronymic->setFilterKeyColumn( 3 );
	m_sfpModelBirthday->setFilterKeyColumn( 4 );
	m_sfpModelAddress->setFilterKeyColumn( 5 );

	m_sfpModelId->setFilterCaseSensitivity( Qt::CaseInsensitive );
	m_sfpModelSurname->setFilterCaseSensitivity( Qt::CaseInsensitive );
	m_sfpModelName->setFilterCaseSensitivity( Qt::CaseInsensitive );
	m_sfpModelPatronymic->setFilterCaseSensitivity( Qt::CaseInsensitive );
	m_sfpModelBirthday->setFilterCaseSensitivity( Qt::CaseInsensitive );
	m_sfpModelAddress->setFilterCaseSensitivity( Qt::CaseInsensitive );
}

void MainWindow::updateTheme( const QString& strThemeFilePath )
{
	if( !strThemeFilePath.isEmpty() )
	{
		QFile styleDark( strThemeFilePath );
		if( styleDark.open( QFile::ReadOnly ) )
			setStyleSheet( styleDark.readAll() );
	}
	else
		setStyleSheet( "" );
}
}
