#include "snapshot_viewer.h"
#include "ui_snapshot_viewer.h"

#include <QResizeEvent>
#include <QStyle>
#include <QDebug>
#include <QScrollBar>
#include <QtMath>

namespace DBPW
{
SnapshotViewer::SnapshotViewer( const QPixmap& pixmap, QWidget *parent ) :
    QDialog( parent ),
    ui( new Ui::SnapshotViewer ),
    m_pixmapSnapshot( pixmap ),
    m_factorScaleInStep( 1.25 ),
    m_factorScaleOutStep( 0.8 ),
    m_factorScaleMax( 3.0 ),
    m_factorScaleMin( 1.0 ),
    m_factorScaleCurrent( 1.0 )
{
	ui->setupUi( this );
	setWindowFlags( windowFlags() & (~Qt::WindowContextHelpButtonHint) | Qt::WindowMaximizeButtonHint );
}

SnapshotViewer::~SnapshotViewer()
{
	delete ui;
}

void SnapshotViewer::wheelEvent( QWheelEvent* event )
{
	if( event->modifiers() == Qt::ControlModifier )
	{
		double angle = event->angleDelta().y();
		if( angle > 0 )
		{
			if( m_factorScaleCurrent < m_factorScaleMax )
				scaleImage( m_factorScaleInStep );
		}
		else if( m_factorScaleCurrent > m_factorScaleMin )
			scaleImage( m_factorScaleOutStep );
	}
	else
		QDialog::wheelEvent( event );
}

void SnapshotViewer::resizeEvent( QResizeEvent* event )
{
	scaleImage( DBL_MAX );
}

void SnapshotViewer::scaleImage( double factor )
{
	if( factor != DBL_MAX )
		m_factorScaleCurrent *= factor;

	QSize sizeNew = size() * m_factorScaleCurrent;
	ui->lPixmap->setPixmap( m_pixmapSnapshot.scaled( sizeNew.width() - 2, sizeNew.height() - 2, Qt::KeepAspectRatio ) );
}

}
