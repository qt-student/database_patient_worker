#include "patient_database.h"

#include <QAbstractItemModel>
#include <QSqlError>
#include <QDebug>
#include <QSqlQuery>
#include <QSqlQueryModel>

namespace DBPW
{
PatientDatabase& PatientDatabase::instance()
{
	static PatientDatabase instance;
	return instance;
}

bool PatientDatabase::addPatient( const QString& strSurname,
                                  const QString& strName,
                                  const QString& strPatronymic,
                                  const QString& strBirthday,
                                  const QString& strAddress )
{
	QSqlQuery query( *m_database );
	if ( !query.prepare( "INSERT INTO patient (surname, name, patronymic, birthday, address) VALUES"
	                     "(:surname, :name, :patronymic, :birthday, :address)" ) )
		return false;
	query.bindValue( ":surname", strSurname );
	query.bindValue( ":name", strName );
	query.bindValue( ":patronymic", strPatronymic );
	query.bindValue( ":birthday", strBirthday );
	query.bindValue( ":address", strAddress );

	return query.exec();
}

bool PatientDatabase::removePatient( const uint32_t idPatient )
{
	QSqlQuery query( *m_database );
	if ( !query.prepare("DELETE FROM patient "
	                    "WHERE id = :id ") )
		return false;
	query.bindValue( ":id", idPatient );
	return query.exec();
}

bool PatientDatabase::dischargePatient( const uint32_t idPatient )
{
	QSqlQuery query( *m_database );
	if ( !query.prepare( "UPDATE medical_examination "
	                     "SET discharge_date = datetime('now', 'localtime') "
	                     "WHERE discharge_date IS NULL AND id_patient = :id" ) )
		return false;
	query.bindValue( ":id", idPatient );

	return query.exec();
}

bool PatientDatabase::adoptPatient(const uint32_t idPatient)
{
	QSqlQuery query( *m_database );
	if ( !query.prepare( "INSERT INTO medical_examination (id_patient, admission_date) "
	                     "VALUES (:id_patient, datetime('now', 'localtime'))") )
		return false;
	query.bindValue( ":id_patient", idPatient );
	return query.exec();
}

QAbstractItemModel* PatientDatabase::getPatientListNotDischarged()
{
	QSqlQuery query( *m_database );
	if( !query.prepare( "SELECT patient.id, surname, name, patronymic, birthday, address FROM patient, medical_examination "
	                    "WHERE patient.id = medical_examination.id_patient AND "
	                    "medical_examination.discharge_date IS NULL" ) )
	{
		return nullptr;
	}
	auto model = executeSelectQuery( query );
	if( model )
	{
		model->setHeaderData( 0, Qt::Horizontal, tr("ID") );
		model->setHeaderData( 1, Qt::Horizontal, tr("Surname") );
		model->setHeaderData( 2, Qt::Horizontal, tr("Name") );
		model->setHeaderData( 3, Qt::Horizontal, tr("Patronymic") );
		model->setHeaderData( 4, Qt::Horizontal, tr("Birthday") );
		model->setHeaderData( 5, Qt::Horizontal, tr("Address") );
	}
	return model;
}


QAbstractItemModel* PatientDatabase::getSnapshotListByPatientId(const uint32_t idSnapshot )
{
	QSqlQuery query( *m_database );

	if( !query.prepare( "SELECT id, image, date FROM snapshot "
	                    "WHERE id_patient = :id_patient" ) )
		return nullptr;
	query.bindValue( ":id_patient", idSnapshot );

	return executeSelectQuery( query );
}

QByteArray PatientDatabase::getSnapshotById(const uint32_t idSnapshot)
{
	QSqlQuery query( *m_database );
	if( !query.prepare( "SELECT image FROM snapshot "
	                    "WHERE id = :id" ) )
		return nullptr;
	query.bindValue( ":id", idSnapshot );
	if ( !executeSelectQuery( query ) )
		return {};
	query.first();
	return query.value(0).toByteArray();
}

QAbstractItemModel* PatientDatabase::executeSelectQuery(QSqlQuery& query)
{
	if( !query.exec() )
		return nullptr;

	QSqlQueryModel* queryModel = new QSqlQueryModel;
	queryModel->setQuery( query );
	return queryModel;
}

bool PatientDatabase::addSnapshot(const uint32_t idPatient, const QByteArray& buffer )
{
	QSqlQuery query( *m_database );
	if( !query.prepare( "INSERT INTO snapshot (id_patient, image, date) VALUES "
	                    "(:id_patient, :image, datetime('now', 'localtime'))" ) )
		return false;
	query.bindValue( ":id_patient", idPatient );
	query.bindValue( ":image", buffer );
	return query.exec();
}

bool PatientDatabase::removeSnapshot( const uint32_t idSnapshot )
{
	QSqlQuery query( *m_database );
	if ( !query.prepare( "DELETE FROM snapshot WHERE id = :id" ) )
		return false;
	query.bindValue(":id", idSnapshot );
	return query.exec();
}

QAbstractItemModel* PatientDatabase::getPatientListAll()
{
	QSqlQuery query( *m_database );
	if( !query.prepare( "SELECT id, surname, name, patronymic, birthday, address FROM patient" ) )
	{
		return nullptr;
	}
	auto model = executeSelectQuery( query );
	if( model )
	{
		model->setHeaderData( 0, Qt::Horizontal, tr("ID") );
		model->setHeaderData( 1, Qt::Horizontal, tr("Surname") );
		model->setHeaderData( 2, Qt::Horizontal, tr("Name") );
		model->setHeaderData( 3, Qt::Horizontal, tr("Patronymic") );
		model->setHeaderData( 4, Qt::Horizontal, tr("Birthday") );
		model->setHeaderData( 5, Qt::Horizontal, tr("Address") );
	}
	return model;
}

QAbstractItemModel* PatientDatabase::getExaminationHistoryByPatient(uint32_t idPatient)
{
	QSqlQuery query( *m_database );
	if( !query.prepare( "SELECT id, admission_date, discharge_date FROM medical_examination "
	                    "WHERE id_patient = :id_patient "
	                    "ORDER BY admission_date DESC" ) )
	{
		return nullptr;
	}
	query.bindValue( ":id_patient", idPatient );
	auto model = executeSelectQuery( query );
	if( model )
	{
		model->setHeaderData( 0, Qt::Horizontal, tr("ID") );
		model->setHeaderData( 1, Qt::Horizontal, tr("Addmission date") );
		model->setHeaderData( 2, Qt::Horizontal, tr("Discharge date") );
	}
	return model;
}

bool PatientDatabase::isConnectionOpen()
{
	return m_database->isOpen();
}

bool PatientDatabase::openConnection( const QString& strDatabaseName )
{
	closeConnection();

	m_database->setDatabaseName( strDatabaseName );
	if( m_database->open() )
	{
		m_database->exec("PRAGMA foreign_keys = ON");
		return true;
	}
	return false;
}

void PatientDatabase::closeConnection()
{
	if ( isConnectionOpen() )
		m_database->close();
}

PatientDatabase::PatientDatabase() :
    m_database( new QSqlDatabase( QSqlDatabase::addDatabase( "QSQLITE" ) ) )
{
}

}
