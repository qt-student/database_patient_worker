#ifndef ADD_PATIENT_FORM_H
#define ADD_PATIENT_FORM_H

#include <QDialog>

namespace Ui
{
class AddPatientForm;
}

namespace DBPW
{
class AddPatientForm :
        public QDialog
{
	Q_OBJECT
public:
	explicit AddPatientForm( QWidget *parent = nullptr );
	~AddPatientForm();

signals:
	void back();

private slots:
	void addButtonClicked();
	void addNextButtonClicked();
	void backButtonClicked();

private:
	Ui::AddPatientForm *ui;
	int m_codeNotAdded;
	bool m_isNewPatientAdded;

	bool hasEmptyFields();
	void clearFields();
	bool addPatient();
};
}

#endif // ADD_PATIENT_FORM_H
