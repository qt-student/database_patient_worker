#include "smart_date_edit.h"
#include <QPushButton>
#include <QStyle>
#include <QStyleOptionSpinBox>
#include <QLineEdit>
#include <QCalendarWidget>
#include <QKeyEvent>


namespace DBPW
{
SmartDateEdit::SmartDateEdit(QWidget *parent) :
    QDateEdit(parent),
    m_pbClearText( nullptr ),
    m_isNull( false )
{
	QPixmap pixmap;
	if( !pixmap.load( ":/icons/clear_icon" ) )
		return;
	pixmap = pixmap.scaled( 13, 13 );
	QIcon icon( pixmap );

	m_pbClearText = new QPushButton( this );
	m_pbClearText->setFlat( true );
	m_pbClearText->setIcon( icon );
	m_pbClearText->setFocusPolicy( Qt::NoFocus );
	m_pbClearText->setFixedSize( pixmap.size() );
	connect( m_pbClearText, &QPushButton::clicked, this, &SmartDateEdit::clearButtonClicked );
}

QDateTime SmartDateEdit::dateTime() const
{
	if ( m_isNull)
		return QDateTime();
	else
		return QDateEdit::dateTime();
}

QDate SmartDateEdit::date() const
{
	if( m_isNull )
		return QDate();
	else
		return QDateEdit::date();
}

QTime SmartDateEdit::time() const
{
	if( m_isNull )
		return QTime();
	else
		return QDateEdit::time();
}

void SmartDateEdit::setDateTime( const QDateTime &dateTime )
{
	if( !dateTime.isValid() )
		setNull( true );
	else
	{
		setNull( false );
		QDateEdit::setDateTime( dateTime );
	}
}


void SmartDateEdit::setDate( const QDate &date )
{
	if ( !date.isValid() )
		setNull( true );
	else
	{
		setNull( false );
		QDateEdit::setDate( date );
	}
}


void SmartDateEdit::setTime(const QTime &time)
{
	if( !time.isValid() )
		setNull(true);
	else
	{
		setNull( false );
		QDateEdit::setTime( time );
	}
}

QSize SmartDateEdit::sizeHint() const
{
	const QSize sz = QDateEdit::sizeHint();
	if ( !m_pbClearText )
		return sz;
	return QSize( sz.width() + m_pbClearText->width() + 3, sz.height() );
}

QSize SmartDateEdit::minimumSizeHint() const
{
	const QSize sz = QDateEdit::minimumSizeHint();
	if ( !m_pbClearText )
		return sz;
	return QSize( sz.width() + m_pbClearText->width() + 3, sz.height() );
}

void SmartDateEdit::showEvent( QShowEvent *event )
{
	QDateEdit::showEvent(event);
	setNull(m_isNull);
}

void SmartDateEdit::resizeEvent( QResizeEvent *event )
{
	if (m_pbClearText)
	{
		QStyleOptionSpinBox opt;
		initStyleOption( &opt );
		opt.subControls = QStyle::SC_SpinBoxUp;

		int left = style()->subControlRect( QStyle::CC_SpinBox, &opt, QStyle::SC_SpinBoxUp, this ).left() - m_pbClearText->width() - 3;
		m_pbClearText->move( left, ( height() - m_pbClearText->height()) / 2 );
	}
	QDateEdit::resizeEvent(event);
}

void SmartDateEdit::keyPressEvent( QKeyEvent *event )
{
	if( (event->key() >= Qt::Key_0) && (event->key() <= Qt::Key_9) && m_isNull )
	{
		setDate( date() );
	}
	if( event->key() == Qt::Key_Tab && m_isNull )
	{
		QAbstractSpinBox::keyPressEvent( event );
		return;
	}
	if( event->key() == Qt::Key_Backspace )
	{
		QLineEdit *edit = findChild<QLineEdit*>();
		if( edit->selectedText() == edit->text() )
		{
			setDate( date() );
			event->accept();
			return;
		}
	}
	QDateEdit::keyPressEvent( event );
}

void SmartDateEdit::mousePressEvent( QMouseEvent *event )
{
	bool isNullSaved = m_isNull;
	QDateEdit::mousePressEvent( event );

	if( isNullSaved && calendarWidget()->isVisible() )
		setDate( date() );
	else if( m_isNull )
		setDate( {} );
}

bool SmartDateEdit::focusNextPrevChild(bool hasNext)
{
	if( m_isNull )
		return QAbstractSpinBox::focusNextPrevChild( hasNext );
	else
		return QDateEdit::focusNextPrevChild( hasNext );
}

void SmartDateEdit::focusInEvent(QFocusEvent* event)
{
	QDateEdit::focusInEvent( event );
	if ( m_isNull )
		setNull( true );
}

QValidator::State SmartDateEdit::validate(QString &input, int &pos) const
{
	if ( m_isNull)
		return QValidator::Acceptable;
	return QDateEdit::validate(input, pos);
}

void SmartDateEdit::clearButtonClicked()
{
	setNull( true );
	clearFocus();
}

void SmartDateEdit::setNull(bool status)
{
	m_isNull = status;
	if ( m_isNull )
	{
		QLineEdit *edit = findChild<QLineEdit*>();
		if ( !edit->text().isEmpty() )
			edit->clear();
	}
	m_pbClearText->setVisible( !m_isNull );
}

void SmartDateEdit::focusOutEvent(QFocusEvent* event)
{
	QDateEdit::focusOutEvent( event );
	if ( m_isNull )
		setNull( true );
}
}
