#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QMainWindow>
#include <QItemSelection>

namespace Ui {
class MainWindow;
}

class QSortFilterProxyModel;
class QStandardItemModel;
class QTranslator;

namespace DBPW
{
class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow( QWidget *parent = nullptr );
	~MainWindow();

protected:
	void changeEvent( QEvent *event ) override;

private slots:
	void patientSelected( const QItemSelection &selected, const QItemSelection &deselected );
	void addPatientAction();
	void removePatientAction();
	void dischargePatientAction();
	void adoptPatientAction();
	void showAllPatientsAction();
	void showNotDischargedPatientsAction();
	void addSnapshotAction();
	void removeSnapshotAction();
	void setRussianLanguageAction();
	void setEnglishLanguageAction();
	void changeThemeButtonClicked();
	void searchNameTextChanged( const QString& text );
	void searchSurnameTextChanged( const QString& text );
	void searchPatronymicTextChanged( const QString& text );
	void searchBirthdayTextChanged( const QDate& date );
	void searchAddressTextChanged( const QString& text );
	void snapshotDoubleClicked( const QModelIndex& item );
	void showPatientContextMenu( const QPoint& pos );
	void showSnapshotContextMenu( const QPoint& pos );

private:
	Ui::MainWindow *ui;
	QTranslator* m_translator;
	QSortFilterProxyModel* m_sfpModelId;
	QSortFilterProxyModel* m_sfpModelSurname;
	QSortFilterProxyModel* m_sfpModelName;
	QSortFilterProxyModel* m_sfpModelPatronymic;
	QSortFilterProxyModel* m_sfpModelBirthday;
	QSortFilterProxyModel* m_sfpModelAddress;
	QStandardItemModel* m_siModelSnapshot;

	uint32_t m_idPatientSelected;
	bool m_isFullPatientList;
	bool m_isLightTheme;

	void loadPatientData( QAbstractItemModel* model );
	void loadExamitationData();
	void loadSnapshotData();
	void initSortFilterProxyModels();
	void updateTheme( const QString& strTheme );
};
}
#endif // MAIN_WINDOW_H
