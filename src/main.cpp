#include <QApplication>

#include <QMessageBox>

#include "main_window.h"
#include "patient_database.h"


int main( int argc, char *argv[] )
{
	QApplication a( argc, argv );

	if ( argc < 2)
	{
		QMessageBox::critical( nullptr, QMessageBox::tr("Error"), QMessageBox::tr("No database path specified!") );
		return -1;
	}

	if( DBPW::PatientDatabase::instance().openConnection( argv[1] ) )
	{
		DBPW::MainWindow w;
		w.resize( 800, 600);
		w.show();
		return a.exec();
	}
	QMessageBox::critical( nullptr, QMessageBox::tr("Error"), QMessageBox::tr("Failed to connect to database!") );
	return -1;
}
