<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AddPatientForm</name>
    <message>
        <location filename="../../src/add_patient_form.ui" line="14"/>
        <source>Add patient dialog</source>
        <translation>Add patient dialog</translation>
    </message>
    <message>
        <location filename="../../src/add_patient_form.ui" line="46"/>
        <source>Add</source>
        <translation>Add</translation>
    </message>
    <message>
        <location filename="../../src/add_patient_form.ui" line="76"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="../../src/add_patient_form.ui" line="89"/>
        <source>Add next</source>
        <translation>Add next</translation>
    </message>
    <message>
        <location filename="../../src/add_patient_form.ui" line="121"/>
        <source>Surname</source>
        <translation>Surname</translation>
    </message>
    <message>
        <location filename="../../src/add_patient_form.ui" line="145"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../src/add_patient_form.ui" line="169"/>
        <source>Patronymic</source>
        <translation>Patronymic</translation>
    </message>
    <message>
        <location filename="../../src/add_patient_form.ui" line="193"/>
        <source>Birthday</source>
        <translation>Birthday</translation>
    </message>
    <message>
        <location filename="../../src/add_patient_form.ui" line="260"/>
        <source>Address</source>
        <translation>Address</translation>
    </message>
    <message>
        <location filename="../../src/add_patient_form.ui" line="292"/>
        <source>Patient adding</source>
        <translation>Patient adding</translation>
    </message>
</context>
<context>
    <name>DBPW::AddPatientForm</name>
    <message>
        <location filename="../../src/add_patient_form.cpp" line="75"/>
        <location filename="../../src/add_patient_form.cpp" line="100"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../../src/add_patient_form.cpp" line="75"/>
        <source>All fields must be filled!</source>
        <translation>All fields must be filled!</translation>
    </message>
    <message>
        <location filename="../../src/add_patient_form.cpp" line="100"/>
        <source>Patient not added!</source>
        <translation>Patient not added!</translation>
    </message>
</context>
<context>
    <name>DBPW::MainWindow</name>
    <message>
        <location filename="../../src/main_window.cpp" line="76"/>
        <location filename="../../src/main_window.cpp" line="86"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../../src/main_window.cpp" line="77"/>
        <source>Surname</source>
        <translation>Surname</translation>
    </message>
    <message>
        <location filename="../../src/main_window.cpp" line="78"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../src/main_window.cpp" line="79"/>
        <source>Patronymic</source>
        <translation>Patronymic</translation>
    </message>
    <message>
        <location filename="../../src/main_window.cpp" line="80"/>
        <source>Birthday</source>
        <translation>Birthday</translation>
    </message>
    <message>
        <location filename="../../src/main_window.cpp" line="81"/>
        <source>Address</source>
        <translation>Address</translation>
    </message>
    <message>
        <location filename="../../src/main_window.cpp" line="87"/>
        <source>Addmission date</source>
        <translation>Addmission date</translation>
    </message>
    <message>
        <location filename="../../src/main_window.cpp" line="88"/>
        <source>Discharge date</source>
        <translation>Discharge date</translation>
    </message>
    <message>
        <location filename="../../src/main_window.cpp" line="127"/>
        <location filename="../../src/main_window.cpp" line="141"/>
        <location filename="../../src/main_window.cpp" line="157"/>
        <location filename="../../src/main_window.cpp" line="208"/>
        <location filename="../../src/main_window.cpp" line="217"/>
        <location filename="../../src/main_window.cpp" line="228"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../../src/main_window.cpp" line="127"/>
        <source>Patient didn&apos;t remove!</source>
        <translation>Patient didn&apos;t remove!</translation>
    </message>
    <message>
        <location filename="../../src/main_window.cpp" line="141"/>
        <source>Patient didn&apos;t discharge!</source>
        <translation>Patient didn&apos;t discharge!</translation>
    </message>
    <message>
        <location filename="../../src/main_window.cpp" line="157"/>
        <source>Patient didn&apos;t adopt!</source>
        <translation>Patient didn&apos;t adopt!</translation>
    </message>
    <message>
        <location filename="../../src/main_window.cpp" line="175"/>
        <source>All patients</source>
        <translation>All patients</translation>
    </message>
    <message>
        <location filename="../../src/main_window.cpp" line="190"/>
        <source>Not discharged patients</source>
        <translation>Not discharged patients</translation>
    </message>
    <message>
        <location filename="../../src/main_window.cpp" line="202"/>
        <source>Open file</source>
        <translation>Open file</translation>
    </message>
    <message>
        <source>Image Files (*.jpg)</source>
        <translation type="vanished">Image Files (*.jpg)</translation>
    </message>
    <message>
        <location filename="../../src/main_window.cpp" line="208"/>
        <source>Failed to upload snapshot!</source>
        <translation>Failed to upload snapshot!</translation>
    </message>
    <message>
        <location filename="../../src/main_window.cpp" line="217"/>
        <source>Failed to add snapshot!</source>
        <translation>Failed to add snapshot!</translation>
    </message>
    <message>
        <location filename="../../src/main_window.cpp" line="228"/>
        <source>Failed to delete snapshot!</source>
        <translation>Failed to delete snapshot!</translation>
    </message>
    <message>
        <location filename="../../src/main_window.cpp" line="362"/>
        <location filename="../../src/main_window.cpp" line="389"/>
        <source>Remove</source>
        <translation>Remove</translation>
    </message>
    <message>
        <location filename="../../src/main_window.cpp" line="366"/>
        <source>Discharge</source>
        <translation>Discharge</translation>
    </message>
    <message>
        <location filename="../../src/main_window.cpp" line="368"/>
        <source>Adopt</source>
        <translation>Adopt</translation>
    </message>
    <message>
        <location filename="../../src/main_window.cpp" line="373"/>
        <location filename="../../src/main_window.cpp" line="394"/>
        <source>Add</source>
        <translation>Add</translation>
    </message>
</context>
<context>
    <name>DBPW::PatientDatabase</name>
    <message>
        <location filename="../../src/patient_database.cpp" line="80"/>
        <location filename="../../src/patient_database.cpp" line="156"/>
        <location filename="../../src/patient_database.cpp" line="179"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../../src/patient_database.cpp" line="81"/>
        <location filename="../../src/patient_database.cpp" line="157"/>
        <source>Surname</source>
        <translation>Surname</translation>
    </message>
    <message>
        <location filename="../../src/patient_database.cpp" line="82"/>
        <location filename="../../src/patient_database.cpp" line="158"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../src/patient_database.cpp" line="83"/>
        <location filename="../../src/patient_database.cpp" line="159"/>
        <source>Patronymic</source>
        <translation>Patronymic</translation>
    </message>
    <message>
        <location filename="../../src/patient_database.cpp" line="84"/>
        <location filename="../../src/patient_database.cpp" line="160"/>
        <source>Birthday</source>
        <translation>Birthday</translation>
    </message>
    <message>
        <location filename="../../src/patient_database.cpp" line="85"/>
        <location filename="../../src/patient_database.cpp" line="161"/>
        <source>Address</source>
        <translation>Address</translation>
    </message>
    <message>
        <location filename="../../src/patient_database.cpp" line="180"/>
        <source>Addmission date</source>
        <translation>Addmission date</translation>
    </message>
    <message>
        <location filename="../../src/patient_database.cpp" line="181"/>
        <source>Discharge date</source>
        <translation>Discharge date</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../src/main_window.ui" line="14"/>
        <source>Database Patient Worker</source>
        <translation>Database Patient Worker</translation>
    </message>
    <message>
        <location filename="../../src/main_window.ui" line="33"/>
        <source>Surname</source>
        <translation>Surname</translation>
    </message>
    <message>
        <location filename="../../src/main_window.ui" line="40"/>
        <source>Snapshots</source>
        <translation>Snapshots</translation>
    </message>
    <message>
        <location filename="../../src/main_window.ui" line="50"/>
        <source>Examinations</source>
        <translation>Examinations</translation>
    </message>
    <message>
        <location filename="../../src/main_window.ui" line="75"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../src/main_window.ui" line="82"/>
        <source>All patients</source>
        <translation>All patients</translation>
    </message>
    <message>
        <location filename="../../src/main_window.ui" line="98"/>
        <source>Search</source>
        <translation>Search</translation>
    </message>
    <message>
        <location filename="../../src/main_window.ui" line="117"/>
        <source>Address</source>
        <translation>Address</translation>
    </message>
    <message>
        <location filename="../../src/main_window.ui" line="167"/>
        <source>Patronymic</source>
        <translation>Patronymic</translation>
    </message>
    <message>
        <source>yyyy-MM-dd</source>
        <translation type="vanished">yyyy-MM-dd</translation>
    </message>
    <message>
        <location filename="../../src/main_window.ui" line="305"/>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location filename="../../src/main_window.ui" line="314"/>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="../../src/main_window.ui" line="324"/>
        <source>Add patient</source>
        <translation>Add patient</translation>
    </message>
    <message>
        <location filename="../../src/main_window.ui" line="329"/>
        <location filename="../../src/main_window.ui" line="349"/>
        <source>Remove</source>
        <translation>Remove</translation>
    </message>
    <message>
        <location filename="../../src/main_window.ui" line="334"/>
        <source>Show all</source>
        <translation>Show all</translation>
    </message>
    <message>
        <location filename="../../src/main_window.ui" line="339"/>
        <source>Show only not discharged</source>
        <translation>Show only not discharged</translation>
    </message>
    <message>
        <location filename="../../src/main_window.ui" line="344"/>
        <source>Add snapshot</source>
        <translation>Add snapshot</translation>
    </message>
    <message>
        <location filename="../../src/main_window.ui" line="354"/>
        <source>Adopt</source>
        <translation>Adopt</translation>
    </message>
    <message>
        <location filename="../../src/main_window.ui" line="359"/>
        <source>Discharge</source>
        <translation>Discharge</translation>
    </message>
    <message>
        <location filename="../../src/main_window.ui" line="364"/>
        <source>Russian</source>
        <translation>Russian</translation>
    </message>
    <message>
        <location filename="../../src/main_window.ui" line="369"/>
        <source>English</source>
        <translation>English</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../../src/main.cpp" line="16"/>
        <location filename="../../src/main.cpp" line="31"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="16"/>
        <source>No database path specified!</source>
        <translation>No database path specified!</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="31"/>
        <source>Failed to connect to database!</source>
        <translation>Failed to connect to database!</translation>
    </message>
</context>
<context>
    <name>SnapshotViewer</name>
    <message>
        <location filename="../../src/snapshot_viewer.ui" line="17"/>
        <source>Snapshot viewer</source>
        <translation>Snapshot viewer</translation>
    </message>
</context>
</TS>
